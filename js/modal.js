class Modal {
    /**
     * @param {HTMLElement} modal - Modal element.
     */
    constructor(modal) {
        this.modal = modal;
    }

    /**
     * Displays an modal that has been removed from the document.
     */
    show() {
        if (this.modal.classList.contains("modal--dismiss"))
            this.modal.classList.remove("modal--dismiss")
    }

    /**
     * Dismissed an modal from the document.
     */
    dismiss() {
        if (!this.modal.classList.contains("modal--dismiss"))
            this.modal.classList.add("modal--dismiss")
    }
}
