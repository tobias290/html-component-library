class Alert {
    /**
     * @param {HTMLElement} alert - Alert element.
     */
    constructor(alert) {
        this.alert = alert;
    }

    /**
     * Displays an alert that has been removed from the document.
     */
    show() {
        if (this.alert.classList.contains("alert--dismiss"))
            this.alert.classList.remove("alert--dismiss")
    }

    /**
     * Dismissed an alert from the document.
     */
    dismiss() {
        if (!this.alert.classList.contains("alert--dismiss"))
            this.alert.classList.add("alert--dismiss")
    }
}
